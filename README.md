Quản lý hàng đợi khách hàng tại Ngân hàng DeveloperBank
=======================================================

Mục tiêu
--------

Phát triển một chương trình console để quản lý hàng đợi của khách hàng rút tiền tại Ngân hàng DeveloperBank. Chương trình cung cấp các chức năng sau:

1.  Thêm khách hàng vào hàng đợi

    -   Người dùng nhập thông tin: Tên khách hàng và Số tiền cần rút.
    -   Nếu Số tiền cần rút lớn hơn 500.000.000, khách hàng được coi là VIP và thêm vào cuối hàng đợi của khách VIP.
    -   Ngược lại, khách hàng thêm vào cuối hàng đợi của khách hàng thường.
    -   Mỗi khách hàng được gán một số thứ tự duy nhất.
    -   Hiển thị số thứ tự và tên của khách hàng trên màn hình.
2.  Gọi tên khách hàng kế tiếp (VIP)

    -   Hiển thị tên và số thứ tự của khách hàng VIP đến lượt rút tiền.
    -   Xóa khách hàng này khỏi hàng đợi.
3.  Gọi tên khách hàng kế tiếp (Thường)

    -   Hiển thị tên và số thứ tự của khách hàng thường đến lượt rút tiền.
    -   Xóa khách hàng này khỏi hàng đợi.
4.  Khách hàng sắp tới

    -   Hiển thị thông tin về 3 khách hàng gần nhất trong mỗi hàng đợi (VIP và thường).
    -   Hiển thị tên và số thứ tự của 3 khách hàng gần nhất, tổng số lượng khách hàng đang chờ của mỗi hằng đợi.
5.  Thống kê

    -   Hiển thị danh sách các khách hàng đã rút tiền.
    -   Tính tổng số tiền đã rút từ danh sách khách hàng.
    -   Hiển thị tổng số tiền còn lại trong hàng đợi.
6.  Thoát khỏi chương trình

    -   Lưu trạng thái hàng đợi vào tệp (nếu cần).
    -   Nếu chương trình bị đột ngột tắt, thông tin hàng đợi sẽ được khôi phục khi chương trình khởi động lại.

Hướng dẫn sử dụng
-----------------

-   Chọn số tương ứng từ menu để thực hiện các chức năng.
-   Nhập thông tin khi được yêu cầu.
-   Theo dõi thông tin trên màn hình và thực hiện các thao tác theo yêu cầu.

Chú ý: Cần hoàn thiện các chức năng trong mã nguồn để triển khai đầy đủ chương trình.