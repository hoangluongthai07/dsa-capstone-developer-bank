﻿public class BankQueueManager
{
    public CustomerQueue EconomyQueue;
    public CustomerQueue VIPQueue;
    public int VIPQueueCount => throw new NotImplementedException();
    public int EconomyQueueCount => throw new NotImplementedException();

    public BankQueueManager(int maxEconomyQueue, int maxVIPQueue)
    {
        EconomyQueue = new CustomerQueue(maxEconomyQueue);
        VIPQueue = new CustomerQueue(maxVIPQueue);
        // TODO: load danh sách khách hàng đã được lưu trong file trước đó
    }

    /// <summary>
    /// 1.Thêm khách hàng vào hàng đợi.
    /// </summary>
    /// <param name="customer"></param>
    public void AddCustomerToTheQueue(Customer customer)
    {
        Console.WriteLine("Thêm khách hàng vào hàng đợi.");
    }

    /// <summary>
    /// 2.Gọi tên khách hàng kế tiếp (VIP)
    /// </summary>
    /// <returns></returns>
    public Customer GetNextVIPCustomer()
    {
        Console.WriteLine("Gọi tên khách hàng kế tiếp (VIP)");
        throw new NotImplementedException();
    }

    /// <summary>
    /// 3.Gọi tên khách hàng kế tiếp (Thường).
    /// </summary>
    /// <returns></returns>
    public Customer GetNextEconomyCustomer()
    {
        Console.WriteLine("3.Gọi tên khách hàng kế tiếp (Thường).");
        throw new NotImplementedException();
    }

    /// <summary>
    /// 4.Khách hàng sắp tới.
    /// </summary>
    public NextReadyCustomer GetNextReadyCustomer()
    {
        Console.WriteLine("4.Khách hàng sắp tới.");
        throw new NotImplementedException();
    }

    /// <summary>
    /// 5.Thống kê
    /// </summary>
    public ReportBank ReportSystem()
    {
        Console.WriteLine("5.Thống kê");
        throw new NotImplementedException();
    }
}

